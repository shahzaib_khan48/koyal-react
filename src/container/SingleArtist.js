import React, { Component } from 'react'
import axios from 'axios'
import { Col, Card, CardImg, CardBody, CardSubtitle, Table } from 'reactstrap';
import Typography from '@material-ui/core/Typography';
import AliceCarousel from 'react-alice-carousel';
import "react-alice-carousel/lib/alice-carousel.css";
import MainSlider from '../component/MainSlider';
import LazyLoad from 'react-lazyload'
import { Link } from "react-router-dom";


export default class SingleArtist extends Component {

    constructor(props) {
        super(props)

        this.state = {
            page_idd : '',
            artistInfo: [],
            artistAlbums: [],
            artistTracks: [],
            TestState: []
        }
    }

    getData = () => {

        axios.get(`http://www.staging.koyal.pk/app_files/web/artist/artist-${this.props.match.params.artistid}.json`)
            .then(response => {
                    console.log(response)
                this.setState({
                    page_idd : this.props.match.params.artistid,
                    artistInfo: response.data.Response,
                    artistAlbums: response.data.Response.Albums,
                    artistTracks: response.data.Response.Tracks
                })
                //console.log(this.state.artistInfo)
            })
            .catch(error => {
                console.log(error)
                this.setState({ errMsg: 'Error Data' })
            })
  
        }

    // testFnf() {
    //     this.forceUpdate();
    // }

    componentDidMount() {
        this.getData()
       //this.testFnf()
    }

    // componentDidUpdate(props, prevState) {

    //     if ( prevState.albumId !== this.props.match.params.artistid) {

    //         this.getData()
    //     }

    // }

    responsive2 = {
        0: { items: 1 },
        1024: { items: 4 },
    }

    render() {
        const { artistInfo, artistAlbums, artistTracks } = this.state

        var divStyle = {
            width: '15%'
        };

        return (
            <>
                <Col xs="3">
                    <Card>
                        <CardImg top width="100%" src={artistInfo.ThumbnailImageWeb} alt={artistInfo.Artist} />
                        <CardBody>
                            <CardSubtitle> <i className="material-icons">
                                share
                            </i>{artistInfo.Shares}</CardSubtitle>
                            <CardSubtitle>
                                <i className="material-icons"> thumb_up </i>
                                {artistInfo.Likes}</CardSubtitle>
                        </CardBody>
                    </Card>
                </Col>
                <Col xs="9">
                    <Typography variant="subtitle2" gutterBottom>
                        Aritist
                       </Typography>
                    <Typography gutterBottom variant="h3" component="h2">
                        {artistInfo.Artist}
                    </Typography>
                    <Typography variant="subtitle2" gutterBottom>
                        By. {artistInfo.Style}
                    </Typography>
                </Col>

                <AliceCarousel
                    items={artistAlbums.map((data, index) => <LazyLoad height={100} offset={[-100, 100]} key={data.index} placeholder={<img src={`https://via.placeholder.com/150`} alt={`hello`} />}>

                        <Link component={Link} to={`/album/` + data.Id + `/` + data.Name}>

                            <MainSlider id={data.Id} image={data.ThumbnailImageWeb} />
                        </Link>

                    </LazyLoad>



                    )}
                    responsive={this.responsive2}
                    autoPlayInterval={2000}
                    autoPlay={true}
                    fadeOutAnimation={true}
                    playButtonEnabled={false}
                    disableAutoPlayOnAction={false}
                />


                <Col xs="12">
                    <Table hover>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Image</th>
                                <th>Title</th>
                                <th>Artist</th>
                                <th>Genre</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                artistTracks.map((data, index) =>
                                    <tr key={index}>
                                        <th scope="row">{index}</th>
                                        <th><img style={divStyle} src={data.ThumbnailImageWeb} /></th>
                                        <td>{data.Name}</td>
                                        <td>{artistInfo.Artist}</td>
                                        <td>{data.LanguageName}</td>
                                    </tr>
                                )
                            }
                        </tbody>
                    </Table>
                </Col>

            </>
        )
    }
}
