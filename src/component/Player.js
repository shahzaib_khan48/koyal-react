import React, { Component } from 'react'
import ReactJWPlayer from 'react-jw-player';
import { withGlobalState } from 'react-globally'
//import { Link } from "react-router-dom";

class Player extends Component {

    constructor(props) {
        super(props)

        this.onVideoLoad = this.onVideoLoad.bind(this);
    }

    onVideoLoad(event) {

        this.props.setGlobalState({
            track_exist: event.item.track_id
        })
    }

    render() {

        return (
            <div className="myAudioPlayer">
                <ReactJWPlayer
                    playerId='MyPlayer'
                    playerScript='https://cdn.jwplayer.com/libraries/PYG4ZTcd.js'
                    playlist={this.props.TrackData}
                    isMuted={true}
                    aspectRatio='inherit'
                    onVideoLoad={this.onVideoLoad}
                />
                <div className="audioPlayerImage">
                    <img src={this.props.TrackImage} alt={this.props.TrackImageName} />
                </div>

            </div>
        )
    }
}

export default withGlobalState(Player)
