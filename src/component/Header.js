import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Grid from '@material-ui/core/Grid';
import { Link } from "react-router-dom";
import SearchView from './SearchView';
import Container from '@material-ui/core/Container';



const Header = props => {

  return (
    <div className="header-parent">
      <AppBar position="fixed" color="inherit">
        <Container fixed maxWidth="lg" className="main-container">
          <Grid container spacing={0}>
            <Grid item md={2}>


              <Link to="/"><img src="assets/koyal-logo.png" alt="koyal-logo" className="logo" /></Link>
            </Grid>
            <Grid item md={5}>
              <div className="desktop-nav">
                <div className="desktop-menu-item first-item">
                  <Link to="/">Home</Link>
                </div>
                <div className="desktop-menu-item">
                  <Link to="/">About Us</Link></div>
                <div className="desktop-menu-item"> <Link to="/">Contact Us</Link></div>
                <div className="desktop-menu-item"> <Link to="/">DMCA</Link></div>
              </div>
            </Grid>

            <Grid item md={5}>
              <div className="searchBox">
                <h5>App Download <i className="material-icons android_icon"> android</i></h5>
                <SearchView />

              </div>
            </Grid>
          </Grid>
        </Container>
      </AppBar>
    </div>
  )
}
export default Header
